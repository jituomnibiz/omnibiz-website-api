const express = require('express'); 

function eRoutes() {
    const router = express.Router();
    router.use("/BOWebsite", require("./api/BOWebsite/BOWebsite.router")(router));
    return router;
}

module.exports = eRoutes;