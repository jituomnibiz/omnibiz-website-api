const config = require("./../config/database");
const sql = require('mssql');
const { detect } = require('detect-browser');
const Utility = require("./Utility");

const _errorDictionary = {
    "0": "Oops! Something went wrong. You can try again later.",
    "1": "Success",
    "2": "Internal Server Error",
    "3": "Authentication Error",
    "4": "Field is Missing or Invalid Format or Max Length Exceeded",
    "5": "Invalid Source"
}
module.exports = {
    GetErrorMessage: (statusCode) => {
        let text = _errorDictionary[statusCode];
        if (text !== undefined)
            return text;
        else
            return null;
    },
    LogMe:async(exGenerated, optionalInformation, procedureName, source, userId, userName,req,res)=>{
        let statusCode, errMsg, errStack, url, referer, browser, os, ip;
        statusCode=res.statusCode;
        errMsg=exGenerated.message;
        errStack=exGenerated.toString();
        url=req.originalUrl;
        referer=req.headers.referer;
        browser=`${detect().name.toString()} ${detect().version.toString()}`;
        os=detect().os.toString();
        ip=req.headers['x-forwarded-for'] || req.connection.remoteAddress;
 
          let clevertapData = {
            d: [
              {
                identity: 1,
                type: "event",
                evtName: "BoApiException",
                evtData: {
                    ErrorMessage: errMsg,
                    StackTrace:errStack,
                    BusinessId:Utility.header_BusinessId(req),
                    ProcedureName:procedureName,
                    source:source
                }
              }
            ]
          }; 
          Utility.cleverTapEvents(0,clevertapData,1);

        sql.on('error', err => {
        })
        sql.connect(config.log).then(pool => {
            return pool.request()
                .input('AppHttpStatus', sql.VarChar(100), statusCode)
                .input('AppErrorMsg', sql.VarChar(500), errMsg)
                .input('AppUrl', sql.VarChar(1000), url)
                .input('AppErrorStack', sql.VarChar(sql.MAX), errStack)
                .input('AppBrowser', sql.VarChar(50), browser)
                .input('AppReferer', sql.VarChar(1000), referer)
                .input('AppOS', sql.VarChar(100), os)
                .input('AppIP', sql.VarChar(100), ip)
                .input('userid', sql.BigInt, userId)
                .input('username', sql.VarChar(100), userName)
                .input('procName', sql.VarChar(100), procedureName)
                .input('errorType', sql.VarChar(100), optionalInformation.toString())
                .input('source', sql.VarChar(100), source)
                .execute('usp_AddApplicationLog')
        }).then((res) => {})
        .catch((error) => {});
    }
}