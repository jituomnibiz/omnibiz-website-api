
const axios = require('axios');

module.exports = {
    Source:'BOWebsite_node-api',
    header_UserId: (req) => {
        return req.headers.userid === undefined ? 0 : req.headers.userid;
    },
    header_BusinessId: (req) => {
        return req.headers.businessid === undefined ? 0 : req.headers.businessid;
    },
    header_Source: (req) => {
        return req.headers.source;
    },
    header_CountryCode: (req) => {
        return req.headers.countrycode === undefined ? '01' : req.headers.countrycode;
    },
    JSONParse:(prm)=>{
        if(prm!==null && prm!==''){
            return JSON.parse(prm)
        }
        else{
            return null;
        }
    },
     RedirectDomain:async (Domain,businessid)=>{
        let obj = { 
            domainName : "vcng.bz", 
            host : `${businessid}.vcng.bz`, 
            forwardsTo : "vcng.bz?bid=" + businessid, 
            type : "masked", 
            meta : "<meta  name=\"viewport\"  content=\"width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no, user-scalable=0\" />" 
        };
        const token = Buffer.from('deepankar:6030d0463c81c078bd972120ad4ec93267262a46', 'utf8').toString('base64')
        let jsonHeader={
            headers: {
                "content-type": "application/json",
                "Authorization": `Basic ${token}`
              }
        };
         await axios.post("https://api.name.com/v4/domains/vcng.bz/url/forwarding", obj ,jsonHeader)
        .then((res) => {
            console.log('Response:'+res);
            })
            .catch((error) => {
                console.log('Error:'+error);
            })
            return 1;
    },
    domainAvailability:(domain,callback)=>{
        let jsonHeader={
            headers: {
                "Content-Type": "application/json"
              }
        };
         axios.get(`https://domain-availability.whoisxmlapi.com/api/v1?apiKey=at_P9PLFhE1rMcVHoFY2krZVoCzFywzT&domainName=${domain}&mode=DNS_AND_WHOIS` ,jsonHeader)
        .then((res) => {
            callback(null,res);
            })
            .catch((error) => {
                callback(error);
            })
    },
    cleverTapEvents:async (userId,obj,flag)=>{
        let jsonHeader={
            headers: {
                "content-type": "application/json",
                "Accept": "application/json",
                "Source": "PWA",
                "userId": userId
              }
        };
         await axios.post(`https://omniapi.omnibiz.com/api/Logger/SendClaverTapEvent?flag=${flag}`, obj ,jsonHeader)
        .then((res) => {
            })
            .catch((error) => {
            })
            return 1;
    }
};