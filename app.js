require("dotenv").config();
const express = require("express");
var cors = require('cors')
const VCAPIResponse = require("./common/VCAPIResponse");
const VCError = require("./common/VCError");
const Utility = require("./common/Utility");
const app = express();
//CORS Middleware
app.use(cors())
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
//CORS Middleware
// app.use(function (err, req, res, next) {
//   //Enabling CORS 
//   //console.log(req.headers['source'])
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, contentType,Content-Type, Accept, Authorization");
//   if (!err) {
//     return next();
//   } else {
//     return res.json({
//       StatusCode: 2,
//       ErrorMessage: err.originalError.message,
//       Result: ''
//     });
//   }
// });
//imports router starting
const BOWebsiteRouter = require("./api/BOWebsite/BOWebsite.router");
app.use("/api", [BOWebsiteRouter]);
//imports router ending
app.use((req, res, next) => {
  const error = new Error("Not found");
  error.status = 400;
  next(error);
});
app.use(errorHandler);
const port = process.env.PORT || 4000;
app.listen(port, () => {
  console.log("Omnibiz API server up and running on PORT :", port);
});

function errorHandler(err, req, res, next) {
  var response = new VCAPIResponse();
  if (res.status(err.status || 500) || req.xhr || err) {
    response.StatusCode = 2;
    response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
    VCError.LogMe(err, JSON.stringify(err), err.procName, Utility.Source, Utility.header_UserId(req), null, req, res);
    response.Result = err.message;
    return res.json(response);
  }
}