const config = require("../../config/database");
const sql = require('mssql');
const Utility = require("../../common/Utility");
module.exports = {

    GetWebsiteDetails: (BusinessId, callBack) => {
        sql.on('error', err => {
            callBack(err);
        })

        sql.connect(config.dr).then(pool => {
            return pool.request()
                .input('BusinessId', sql.Int, BusinessId)
                .execute('GetWebsiteDetails')
        }).then(result => {
            let dataDB = {};
            if(result.recordsets[0].length>0){
                dataDB.BusinessId = result.recordsets[0][0]["businessid"];
                dataDB.Banner = Utility.JSONParse(result.recordsets[0][0]["Banner"]);
                dataDB.WebsiteOverview = Utility.JSONParse(result.recordsets[0][0]["WebsiteOverview"]);
                dataDB.Reviews = Utility.JSONParse(result.recordsets[0][0]["Reviews"]);
                dataDB.Faqs = Utility.JSONParse(result.recordsets[0][0]["Faqs"]);
                dataDB.Store = Utility.JSONParse(result.recordsets[0][0]["Store"]);
                dataDB.Photos = Utility.JSONParse(result.recordsets[0][0]["Photos"]);
                dataDB.TeamMembers = Utility.JSONParse(result.recordsets[0][0]["TeamMembers"]);
                dataDB.ColorCode = result.recordsets[0][0]["ColorCode"];
                dataDB.Announcements = Utility.JSONParse(result.recordsets[0][0]["Announcements"]);
            }
            return callBack(null, dataDB);
        }).catch(err => {
            callBack(err);
        })
    },
    GetWebsitePastproject: (BusinessId, callBack) => {
        sql.on('error', err => {
            callBack(err);
        })
        sql.connect(config.dr).then(pool => {
            return pool.request()
                .input('BusinessId', sql.Int, BusinessId)
                .execute('GetWesitePasproject')
        }).then(result => {
            return callBack(null, Utility.JSONParse(result.recordsets[0][0]["Pastprojects"]));
        }).catch(err => {
            callBack(err);
        })
    },
    GetWebsitePastProjectDetails: (PastProjectID, callBack) => {
        sql.on('error', err => {
            callBack(err);
        })
        sql.connect(config.dr).then(pool => {
            return pool.request()
                .input('PastProjectID', sql.Int, PastProjectID)
                .execute('GetWebsitePastProjectDetails')
        }).then(result => {
            var getWebsitePastProjectDetails = "";
            if (result.recordsets[0].length > 0)
                getWebsitePastProjectDetails = result.recordsets[0][0];
            getWebsitePastProjectDetails.PastProjectDetails = result.recordsets[1] !== undefined && result.recordsets[1].length > 0 ? result.recordsets[1] : [];
            return callBack(null, getWebsitePastProjectDetails);
        }).catch(err => {
            callBack(err);
        })
    },
    GetWebsiteStore: (BusinessId, prm, callBack) => {
        sql.on('error', err => {
            callBack(err);
        })
        sql.connect(config.dr).then(pool => {
            return pool.request()
                .input('BusinessId', sql.Int, BusinessId)
                .input('PageNo', sql.Int, prm.PageNo)
                .input('RecordPerPage', sql.Int, prm.RecordPerPage)
                .execute('GetWesiteStore')
        }).then(result => {
            return callBack(null, Utility.JSONParse(result.recordsets[0][0]["Store"]));
        }).catch(err => {
            callBack(err);
        })
    },
    GetWebsiteProductDetail: (BusinessId, productid, callBack) => {
        sql.on('error', err => {
            callBack(err);
        })
        sql.connect(config.dr).then(pool => {
            return pool.request()
                .input('BusinessId', sql.Int, BusinessId)
                .input('productid', sql.Int, productid)
                .execute('GetWebsiteProductDetail')
        }).then(result => {
            var getWebsiteProductDetail = "";
            if (result.recordsets[0].length > 0)
                getWebsiteProductDetail = result.recordsets[0][0];
            getWebsiteProductDetail.ProductDetails = result.recordsets[1] !== undefined && result.recordsets[1].length > 0 ? result.recordsets[1] : [];
            return callBack(null, getWebsiteProductDetail);
        }).catch(err => {
            callBack(err);
        })
    },
    AddWebsiteMaster: (addWebsiteMaster, BusinessId, callBack) => {
        sql.on('error', err => {
            callBack(err);
        })
        sql.connect(config.dr).then(pool => {
            return pool.request()
                .input('BusinessId', sql.Int, BusinessId)
                .input('DomainName', sql.VarChar(124), `${BusinessId}.vcng.bz`)
                .input('clientDomainName', sql.VarChar(124), addWebsiteMaster.clientDomainName)
                .input('activedate', sql.DateTime, addWebsiteMaster.Activedate)
                .input('expirydate', sql.DateTime, addWebsiteMaster.Expirydate)
                .input('colorCode', sql.VarChar(64), addWebsiteMaster.ColorCode)
                .input('WebSiteID', sql.Int, addWebsiteMaster.WebsiteId)
                .input('flag', sql.Int, addWebsiteMaster.Flag)
                .input('type', sql.Int, addWebsiteMaster.Type)
                .input('IsclientDomain', sql.Int, addWebsiteMaster.IsDomainExists)
                .output('error', sql.Int, 4)
                .output('subdomain', sql.VarChar(124), null)
                .execute('AddWebsiteMaster')
        }).then(result => {
            return callBack(null, result.output);
        }).catch(err => {
            callBack(err);
        })
    },
    GetBusinessID: (Domain, callBack) => {
        sql.on('error', err => {
            callBack(err);
        })
        sql.connect(config.dr).then(pool => {
            return pool.request()
                .input('DomainName', sql.VarChar(124), Domain)
                .execute('GetBusinessID')
        }).then(result => {
            return callBack(null, result.recordsets[0][0].BusinessID);
        }).catch(err => {
            callBack(err);
        })
    },
    GetWebsiteReview: (BusinessId, prm, callBack) => {
        sql.on('error', err => {
            callBack(err);
        })
        sql.connect(config.dr).then(pool => {
            return pool.request()
                .input('BusinessId', sql.Int, BusinessId)
                .input('PageNo', sql.Int, prm.pageno)
                .input('RecordPerPage', sql.Int, prm.recordperpage)
                .execute('GetWebsiteReview')
        }).then(result => {
            return callBack(null, Utility.JSONParse(result.recordsets[0][0]["Reviews"]));
        }).catch(err => {
            callBack(err);
        })
    },
    GetWebsitephotos: (BusinessId, prm, callBack) => {
        sql.on('error', err => {
            callBack(err);
        })
        sql.connect(config.dr).then(pool => {
            return pool.request()
                .input('BusinessId', sql.Int, BusinessId)
                .input('PageNumber', sql.Int, prm.pageno)
                .input('RowsPerPage', sql.Int, prm.recordperpage)
                .execute('GetWebsitephotos')
        }).then(result => {
            return callBack(null, Utility.JSONParse(result.recordsets[0][0]["Photos"]));
        }).catch(err => {
            callBack(err);
        })
    },
    AddBusinessContactUS: (addBusinessContactUS, BusinessId, callBack) => {
        sql.on('error', err => {
            callBack(err);
        })
        sql.connect(config.dr).then(pool => {
            return pool.request()
                .input('businessid', sql.Int, BusinessId)
                .input('Username', sql.VarChar(64), addBusinessContactUS.UserName)
                .input('Userphone', sql.VarChar(20), addBusinessContactUS.UserPhone)
                .input('useremail', sql.VarChar(124), addBusinessContactUS.UserEmail)
                .input('Message', sql.VarChar(1024), addBusinessContactUS.Message)
                .output('error', sql.Int, 4)
                .output('userId', sql.Int, 4)
                .execute('AddBusinessContactUS')
        }).then(result => {
            return callBack(null, result.output);
        }).catch(err => {
            callBack(err);
        })
    },
    GetWebsiteDomain: (BusinessId, callBack) => {
        sql.on('error', err => {
            callBack(err);
        })
        sql.connect(config.dr).then(pool => {
            return pool.request()
                .input('BusinessId', sql.Int, BusinessId)
                .execute('GetWebsiteDomain')
        }).then(result => {
            return callBack(null, result.recordsets[0][0]);
        }).catch(err => {
            callBack(err);
        })
    },
    AddWebsitephoneLog: (addWebsitephoneLog, BusinessId, callBack) => {
        sql.on('error', err => {
            callBack(err);
        })
        sql.connect(config.dr).then(pool => {
            return pool.request()
                .input('businessid', sql.Int, BusinessId)
                .input('Domainname', sql.VarChar(512), addWebsitephoneLog.DomainName)
                .input('SkuID', sql.Int, addWebsitephoneLog.SkuId)
                .input('Source', sql.VarChar(48), addWebsitephoneLog.Source)
                .output('error', sql.Int, 4)
                .execute('AddWebsitephoneLog')
        }).then(result => {
            return callBack(null, result.output);
        }).catch(err => {
            callBack(err);
        })
    },
    GetBusinessTeamMember: (BusinessId, callBack) => {
        sql.on('error', err => {
            callBack(err);
        })
        sql.connect(config.dr).then(pool => {
            return pool.request()
                .input('BusinessId', sql.Int, BusinessId)
                .execute('GetBusinessTeamMember_PWA')
        }).then(result => {
            return callBack(null, result.recordsets[0][0]);
        }).catch(err => {
            callBack(err);
        })
    },
    GetWebsiteAnnouncements: (BusinessId, prm, callBack) => {
        sql.on('error', err => {
            callBack(err);
        })
        sql.connect(config.dr).then(pool => {
            return pool.request()
                .input('BusinessId', sql.Int, BusinessId)
                .input('pageno', sql.Int, prm.PageNo)
                .input('RecordPerpage', sql.Int, prm.RecordPerPage)
                .execute('GetWesiteAnnouncements')
        }).then(result => {
            return callBack(null, Utility.JSONParse(result.recordsets[0][0]["announcement"]));
        }).catch(err => {
            callBack(err);
        })
    },
    GetWebsiteAnnouncementsDetail: (prm, callBack) => {
        sql.on('error', err => {
            callBack(err);
        })
        sql.connect(config.dr).then(pool => {
            return pool.request()
                .input('Updateid', sql.Int, prm.Updateid)
                .execute('GetWebsiteAnnouncementsDetail')
        }).then(result => {
            return callBack(null, Utility.JSONParse(result.recordsets[0][0]["announcement"]));
        }).catch(err => {
            callBack(err);
        })
    },
    AddWebsiteSubscription: (addWebsiteSubscription, BusinessId, callBack) => {
        sql.on('error', err => {
            callBack(err);
        })
        sql.connect(config.dr).then(pool => {
            return pool.request()
                .input('businessid', sql.Int, BusinessId)
                .input('PlanID', sql.Int, addWebsiteSubscription.PlanID)
                .input('amount', sql.Int, addWebsiteSubscription.amount)
                .output('error', sql.Int, 4)
                .execute('AddWebsiteSubscription')
        }).then(result => {
            return callBack(null, result.output);
        }).catch(err => {
            callBack(err);
        })
    },
    GetWebsitePortfolio: (prm, callBack) => {
        sql.on('error', err => {
            callBack(err);
        })
        sql.connect(config.dwr).then(pool => {
            return pool.request()
                .input('categoryname', sql.VarChar(48), prm.Catagory)
                .execute('GetWebsitePortfolio')
        }).then(result => {
            var getWebsitePortfolio = {};
            getWebsitePortfolio.webisteInfos = result.recordsets[0].length > 0 ? result.recordsets[0] : [];
            getWebsitePortfolio.NoOfWebsite = result.recordsets[1] !== undefined && result.recordsets[1].length > 0 ? result.recordsets[1][0].NoOfWebsite : 0;
            getWebsitePortfolio.Categorylist = result.recordsets[2] !== undefined && result.recordsets[2].length > 0 ? result.recordsets[2] : [];
            return callBack(null, getWebsitePortfolio);
        }).catch(err => {
            callBack(err);
        })
    }
}