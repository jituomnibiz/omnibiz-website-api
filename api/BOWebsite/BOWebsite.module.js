class GetWebsitePastProjectDetailsPrm{
    constructor(prm){
       this.PastProjectID=prm.query.PastProjectID!==undefined?prm.query.PastProjectID:0;
    }
   }
class GetWebsiteStorePrm{
    constructor(prm){
       this.PageNo=prm.query.PageNo!==undefined?prm.query.PageNo:1;
       this.RecordPerPage=prm.query.RecordPerPage!==undefined?prm.query.RecordPerPage:10;
    }
   }
class GetWebsiteProductDetailPrm{
    constructor(prm){
       this.productid=prm.query.productid!==undefined?prm.query.productid:0;
    }
   }
class AddWebsiteMasterPrm{
    constructor(prm){
       this.clientDomainName=prm.body.clientDomainName!==undefined?prm.body.clientDomainName:'';
       this.Activedate=prm.body.Activedate!==undefined?prm.body.Activedate:null;
       this.Expirydate=prm.body.Expirydate!==undefined?prm.body.Expirydate:null;
       this.ColorCode=prm.body.ColorCode!==undefined?prm.body.ColorCode:'';
       this.WebsiteId=prm.body.WebsiteId!==undefined?prm.body.WebsiteId:0;
       this.Flag=prm.body.Flag!==undefined?prm.body.Flag:0;
       this.Type=prm.body.Type!==undefined?prm.body.Type:0;
       this.IsDomainExists=prm.body.IsDomainExists!==undefined?prm.body.IsDomainExists:0;
    }
   }
class GetBusinessIDPrm{
    constructor(prm){
       this.Domain=prm.query.Domain!==undefined?prm.query.Domain:'';
    }
   }
class GetWebsiteReviewPrm{
    constructor(prm){
       this.pageno=prm.query.pageno!==undefined?prm.query.pageno:1;
       this.recordperpage=prm.query.recordperpage!==undefined?prm.query.recordperpage:10;
    }
   }
class GetWebsitephotosPrm{
    constructor(prm){
       this.pageno=prm.query.pageno!==undefined?prm.query.pageno:1;
       this.recordperpage=prm.query.recordperpage!==undefined?prm.query.recordperpage:10;
    }
   }
class AddBusinessContactUSPrm{
    constructor(prm){
       this.UserName=prm.body.UserName!==undefined?prm.body.UserName:'';
       this.UserPhone=prm.body.UserPhone!==undefined?prm.body.UserPhone:'';
       this.UserEmail=prm.body.UserEmail!==undefined?prm.body.UserEmail:'';
       this.Message=prm.body.Message!==undefined?prm.body.Message:'';
    }
   }
class AddWebsitephoneLogPrm{
    constructor(prm){
       this.DomainName=prm.body.DomainName!==undefined?prm.body.DomainName:'';
       this.SkuId=prm.body.SkuId!==undefined?prm.body.SkuId:0;
       this.Source=prm.body.Source!==undefined?prm.body.Source:'BO-website';
    }
   }
class CheckDomainAvailabilityPrm{
    constructor(prm){
       this.domain=prm.query.domain!==undefined?prm.query.domain:'';
    }
   }
class GetWebsiteAnnouncementsPrm{
    constructor(prm){
       this.PageNo=prm.query.PageNo!==undefined?prm.query.PageNo:1;
       this.RecordPerPage=prm.query.RecordPerPage!==undefined?prm.query.RecordPerPage:10;
    }
   }
class GetWebsiteAnnouncementsDetailPrm{
    constructor(prm){
       this.Updateid=prm.query.Updateid!==undefined?prm.query.Updateid:0;
    }
   }
class AddWebsiteSubscriptionPrm{
    constructor(prm){
       this.PlanID=prm.body.PlanID!==undefined?prm.body.PlanID:0;
       this.amount=prm.body.amount!==undefined?prm.body.amount:0;
    }
   }
class GetWebsitePortfolioPrm{
    constructor(prm){
       this.Catagory=prm.query.Catagory!==undefined?prm.query.Catagory:'';
    }
   }
   module.exports = {
       GetWebsitePastProjectDetailsPrm:GetWebsitePastProjectDetailsPrm,
       GetWebsiteStorePrm:GetWebsiteStorePrm,
       GetWebsiteProductDetailPrm:GetWebsiteProductDetailPrm,
       AddWebsiteMasterPrm:AddWebsiteMasterPrm,
       GetBusinessIDPrm:GetBusinessIDPrm,
       GetWebsiteReviewPrm:GetWebsiteReviewPrm,
       GetWebsitephotosPrm:GetWebsitephotosPrm,
       AddBusinessContactUSPrm:AddBusinessContactUSPrm,
       AddWebsitephoneLogPrm:AddWebsitephoneLogPrm,
       CheckDomainAvailabilityPrm:CheckDomainAvailabilityPrm,
       GetWebsiteAnnouncementsPrm:GetWebsiteAnnouncementsPrm,
       GetWebsiteAnnouncementsDetailPrm:GetWebsiteAnnouncementsDetailPrm,
       AddWebsiteSubscriptionPrm:AddWebsiteSubscriptionPrm,
       GetWebsitePortfolioPrm:GetWebsitePortfolioPrm
   };