const sleep = require('thread-sleep');
const BOWebsite = require('./BOWebsite.service');
const VCAPIResponse = require('../../common/VCAPIResponse');
const Utility = require('../../common/Utility');
const VCError = require('../../common/VCError');
const BOWebsiteModule=require('./BOWebsite.module');

module.exports = {
  GetWebsiteDetails: (req, res) => {
    var response=new VCAPIResponse();
    const BusinessId = Utility.header_BusinessId(req);
    BOWebsite.GetWebsiteDetails(BusinessId, (err, results) => {
      if (err) {
        response.StatusCode = 2;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        VCError.LogMe(err,JSON.stringify(err),err.procName,Utility.Source,Utility.header_UserId(req),null,req,res);
        response.Result = err.message;
      } else {
        response.StatusCode = 1;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = results;
      }
      return res.json(response);
    });
  },
  GetWebsitePastproject: (req, res) => {
    var response=new VCAPIResponse();
    const BusinessId = Utility.header_BusinessId(req);
    BOWebsite.GetWebsitePastproject(BusinessId, (err, results) => {
      if (err) {
        response.StatusCode = 2;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = err.message;
      } else {
        response.StatusCode = 1;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = results;
      }
      return res.json(response);
    });
  },
  GetWebsitePastProjectDetails: (req, res) => {
    var response=new VCAPIResponse();
    let prm=new BOWebsiteModule.GetWebsitePastProjectDetailsPrm(req);
    BOWebsite.GetWebsitePastProjectDetails(prm.PastProjectID, (err, results) => {
      if (err) {
        response.StatusCode = 2;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = err.message;
      } else {
        response.StatusCode = 1;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = results;
      }
      return res.json(response);
    });
  },
  GetWebsiteStore: (req, res) => {
    var response=new VCAPIResponse();
    const BusinessId = Utility.header_BusinessId(req);
    let prm=new BOWebsiteModule.GetWebsiteStorePrm(req);
    BOWebsite.GetWebsiteStore(BusinessId,prm, (err, results) => {
      if (err) {
        response.StatusCode = 2;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = err.message;
      } else {
        response.StatusCode = 1;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = results;
      }
      return res.json(response);
    });
  },
  GetWebsiteProductDetail: (req, res) => {
    var response=new VCAPIResponse();
    const BusinessId = Utility.header_BusinessId(req);
    let prm=new BOWebsiteModule.GetWebsiteProductDetailPrm(req);
    BOWebsite.GetWebsiteProductDetail(BusinessId,prm.productid, (err, results) => {
      if (err) {
        response.StatusCode = 2;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = err.message;
      } else {
        response.StatusCode = 1;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = results;
      }
      return res.json(response);
    });
  },
  AddWebsiteMaster: (req, res) => {
    var response=new VCAPIResponse();
    const BusinessId = Utility.header_BusinessId(req);
    let prm=new BOWebsiteModule.AddWebsiteMasterPrm(req);
    var resResult={};
    BOWebsite.AddWebsiteMaster(prm,BusinessId, (err, results) => {
      if (err) {
        response.StatusCode = 2;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = err.message;
      } else {
        resResult=results;
         if (results.error == 2){
          prm.DomainName = BusinessId + prm.DomainName;
          BOWebsite.AddWebsiteMaster(prm,BusinessId, (innerErr, innerResults) => {
          resResult=innerResults;
         });
         }
         if (resResult.error == 1 && prm.Flag == 1 && prm.DomainName != "www.vcng.bz"){
             Utility.RedirectDomain(prm.DomainName,BusinessId);
             sleep(5000);
             response.StatusCode = 1;
             response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
             response.Result={SubDomain:resResult.subdomain,IsSubDomainWorking:1};
         }
         else if (prm.DomainName == "www.vcng.bz"){
           response.StatusCode=4;
           response.Result = "Subdomain Cannot be " + prm.DomainName;
         }
      }
      return res.json(response);
    });
  },
  GetBusinessID: (req, res) => {
    var response=new VCAPIResponse();
    const Domain = req.query.Domain;
    let prm=new BOWebsiteModule.GetBusinessIDPrm(req);
    BOWebsite.GetBusinessID(prm.Domain, (err, results) => {
      if (err) {
        response.StatusCode = 2;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = err.message;
      } else {
        response.StatusCode = 1;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = results!=null && results!=''?results:0;
      }
      return res.json(response);
    });
  },
  GetWebsiteReview: (req, res) => {
    var response=new VCAPIResponse();
    const BusinessId = Utility.header_BusinessId(req);
    let prm=new BOWebsiteModule.GetWebsiteReviewPrm(req);
    BOWebsite.GetWebsiteReview(BusinessId,prm, (err, results) => {
      if (err) {
        response.StatusCode = 2;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = err.message;
      } else {
        response.StatusCode = 1;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = results;
      }
      return res.json(response);
    });
  },
  GetWebsitephotos: (req, res) => {
    var response=new VCAPIResponse();
    const BusinessId = Utility.header_BusinessId(req);
    let prm=new BOWebsiteModule.GetWebsitephotosPrm(req);
    BOWebsite.GetWebsitephotos(BusinessId,prm, (err, results) => {
      if (err) {
        response.StatusCode = 2;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = err.message;
      } else {
        response.StatusCode = 1;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = results;
      }
      return res.json(response);
    });
  },
  AddBusinessContactUS: (req, res) => {
    var response=new VCAPIResponse();
    const BusinessId = Utility.header_BusinessId(req);
    let prm=new BOWebsiteModule.AddBusinessContactUSPrm(req);
    BOWebsite.AddBusinessContactUS(prm,BusinessId, (err, results) => {
      if (err) {
        response.StatusCode = 2;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = err.message;
      } else {
        response.StatusCode = results.error;
        if (results.error == 1 && results.userId > 0){
          var jsonObj={"d": [
            {
              "identity":results.userId,
              "type":"event",
              "evtName":"WebsiteBusinessContactUs",
              "evtData" : { 
                "Businessid":BusinessId,
                "UserName":prm.UserName,
                "UserEmail": prm.UserEmail,
                "UserPhone":prm.UserPhone,
                "Message" : prm.Message 
                }
            }
          ]};    
          Utility.cleverTapEvents(results.userId,jsonObj,0);
        }
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
      }
      return res.json(response);
    });
  },
  GetWebsiteDomain: (req, res) => {
    var response=new VCAPIResponse();
    const BusinessId = Utility.header_BusinessId(req);
    BOWebsite.GetWebsiteDomain(BusinessId, (err, results) => {
      if (err) {
        response.StatusCode = 2;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = err.message;
      } else {
        response.StatusCode = results.error;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = results;
      }
      return res.json(response);
    });
  },
  AddWebsitephoneLog: (req, res) => {
    var response=new VCAPIResponse();
    const BusinessId = Utility.header_BusinessId(req);
    let prm=new BOWebsiteModule.AddWebsitephoneLogPrm(req);
    BOWebsite.AddWebsitephoneLog(prm,BusinessId, (err, results) => {
      if (err) {
        response.StatusCode = 2;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = err.message;
      } else {
        response.StatusCode = results.error===1?1:2;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
      }
      return res.json(response);
    });
  },
  GetBusinessTeamMember: (req, res) => {
    var response=new VCAPIResponse();
    const BusinessId = Utility.header_BusinessId(req);
    BOWebsite.GetBusinessTeamMember(BusinessId, (err, results) => {
      if (err) {
        response.StatusCode = 2;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = err.message;
      } else {
        response.StatusCode = results.error;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = results;
      }
      return res.json(response);
    });
  },
  CheckDomainAvailability: (req, res) => {
    var response=new VCAPIResponse();
    const domain=req.query.domain;
    let prm=new BOWebsiteModule.CheckDomainAvailabilityPrm(req);
    Utility.domainAvailability(prm.domain, (err, results) => {
      if (err) {
        response.StatusCode = 2;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = err.message;
      } else {
        let obj={};
        if(results.StatusCode=='OK' && results.data.DomainInfo.domainAvailability=='AVAILABLE'){
          obj={
            availability_status:1,
            message:'Domain is Available',
            suggested_available_domains:''
          }
        } else{
          obj={
            availability_status:0,
            message:`Domain name ${domain} is not available`,
            suggested_available_domains:''
          }
        }
        response.StatusCode = 1;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = obj;
      }
      return res.json(response);
    });
  },
  GetWebsiteAnnouncements: (req, res) => {
    var response=new VCAPIResponse();
    const BusinessId = Utility.header_BusinessId(req);
    let prm=new BOWebsiteModule.GetWebsiteAnnouncementsPrm(req);
    BOWebsite.GetWebsiteAnnouncements(BusinessId,prm, (err, results) => {
      if (err) {
        response.StatusCode = 2;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = err.message;
      } else {
        response.StatusCode = 1;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = results;
      }
      return res.json(response);
    });
  },
  GetWebsiteAnnouncementsDetail: (req, res) => {
    var response=new VCAPIResponse();
    let prm=new BOWebsiteModule.GetWebsiteAnnouncementsDetailPrm(req);
    BOWebsite.GetWebsiteAnnouncementsDetail(prm, (err, results) => {
      if (err) {
        response.StatusCode = 2;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = err.message;
      } else {
        response.StatusCode = 1;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = results;
      }
      return res.json(response);
    });
  },
  AddWebsiteSubscription: (req, res) => {
    var response=new VCAPIResponse();
    const BusinessId = Utility.header_BusinessId(req);
    let prm=new BOWebsiteModule.AddWebsiteSubscriptionPrm(req);
    BOWebsite.AddWebsiteSubscription(prm,BusinessId, (err, results) => {
      if (err) {
        response.StatusCode = 2;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = err.message;
      } else {
        response.StatusCode = results.error===1?1:0;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
      }
      return res.json(response);
    });
  },
  GetWebsitePortfolio: (req, res) => {
    var response=new VCAPIResponse();
    let prm=new BOWebsiteModule.GetWebsitePortfolioPrm(req);
    BOWebsite.GetWebsitePortfolio(prm, (err, results) => {
      if (err) {
        response.StatusCode = 2;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = err.message;
      } else {
        response.StatusCode = 1;
        response.ErrorMessage = VCError.GetErrorMessage(response.StatusCode);
        response.Result = results;
      }
      return res.json(response);
    });
  }
}