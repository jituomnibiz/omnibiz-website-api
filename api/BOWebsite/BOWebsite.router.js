const router = require("express").Router();
const BOWebsite = require("./BOWebsite.controller");

router.get("/BOWebsite/GetWebsiteDetails", BOWebsite.GetWebsiteDetails);
router.get("/BOWebsite/GetWebsitePastproject", BOWebsite.GetWebsitePastproject);
router.get("/BOWebsite/GetWebsitePastProjectDetails", BOWebsite.GetWebsitePastProjectDetails);//PastProjectID
router.get("/BOWebsite/GetWebsiteStore", BOWebsite.GetWebsiteStore);//PageNo,RecordPerPage
router.get("/BOWebsite/GetWebsiteProductDetail", BOWebsite.GetWebsiteProductDetail);//productid
router.post("/BOWebsite/AddWebsiteMaster", BOWebsite.AddWebsiteMaster);
router.get("/BOWebsite/GetBusinessID", BOWebsite.GetBusinessID);//Domain
router.get("/BOWebsite/GetWebsiteReview", BOWebsite.GetWebsiteReview);//pageno,recordperpage
router.get("/BOWebsite/GetWebsitephotos", BOWebsite.GetWebsitephotos);//pageno,recordperpage
router.post("/BOWebsite/AddBusinessContactUS", BOWebsite.AddBusinessContactUS);
router.get("/BOWebsite/GetWebsiteDomain", BOWebsite.GetWebsiteDomain);
router.post("/BOWebsite/AddWebsitephoneLog", BOWebsite.AddWebsitephoneLog);
router.get("/BOWebsite/GetBusinessTeamMember", BOWebsite.GetBusinessTeamMember);
router.get("/BOWebsite/CheckDomainAvailability", BOWebsite.CheckDomainAvailability);//domain
router.get("/BOWebsite/GetWebsiteAnnouncements", BOWebsite.GetWebsiteAnnouncements);//PageNo,RecordPerPage
router.get("/BOWebsite/GetWebsiteAnnouncementsDetail", BOWebsite.GetWebsiteAnnouncementsDetail);//Updateid
router.post("/BOWebsite/AddWebsiteSubscription", BOWebsite.AddWebsiteSubscription);
router.get("/BOWebsite/GetWebsitePortfolio", BOWebsite.GetWebsitePortfolio);//Catagory

module.exports = router;